FROM ubuntu

RUN apt-get update \
	&& apt-get install -y boinc-client

RUN apt-get install -y nano

ADD entrypoint.sh /

RUN chmod +x /entrypoint.sh

VOLUME /var/lib/boinc-client/

ENTRYPOINT ["/entrypoint.sh"]
