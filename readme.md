# BOINC Client Dockerfile

## Build

```bash
docker build -t boinc:0.7 .
```

## Run

```bash
docker run -dit --restart=always --hostname hostname.com --name boinc1 -p 31416:31416 boinc:0.7
```

## Account Manager

```bash
docker exec boinc1 boinccmd --join_acct_mgr URL user password
```


## Sync /w Account Manager

1. Get project info (after joining account manager)

```bash
docker exec boinc1 boinccmd --get_simple_gui_info
```

2. Manually sync BOINC client with account manager (if it doesn't work out of the box)
```bash
docker exec boinc1 boinccmd --project URL update
docker exec boinc1 boinccmd --project URL allowmorework
docker exec boinc1 boinccmd --project URL resume
```

Now the info query (see 1.) should list tasks as EXECUTING and warnings in the account 
manager that the client is out of sync should disappear (might take a few minutes until 
the first taskt have been computed / have started computing). If not, repeat step 2. or 
get creative.
